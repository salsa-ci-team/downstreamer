#! /usr/bin/python3
import os
import subprocess
import gitlab
import docker
import logging
import argparse
from functools import lru_cache

args = None

SALSA_PIPELINE_PASSWORD = os.environ.get('SALSA_PIPELINE_PASSWORD')
GITLAB_PRIVATE_TOKEN = os.environ.get('GITLAB_TOKEN')
PROTECTED_VARIABLES = [SALSA_PIPELINE_PASSWORD, GITLAB_PRIVATE_TOKEN]

LIST_FILE = 'repos.list'
UPSTREAM_BRANCH = 'upstream-update'
GITLAB_URL = 'https://salsa.debian.org'
DOCKER_IMAGE = 'registry.salsa.debian.org/salsa-ci-team/downstreamer'
SALSA_PIPELINE_BOT_LABEL = 'update-proposal'
ISSUE_BODY = '''A problem was found when importing upstream.

This was the run output:
```
{}
```
'''

SALSA = gitlab.Gitlab(GITLAB_URL, private_token=GITLAB_PRIVATE_TOKEN)
DOCKER = docker.from_env()

# Exit codes
EC_UPDATE_AVAILABLE = 0
EC_UP_TO_DATE = 4


def sanitize_string(string):
    """Remove protected variables from a given string."""
    for var in PROTECTED_VARIABLES:
        string = string.replace(var, '%hidden%')
    return string


class Container():
    """Container class."""

    def __init__(self, name):
        """Init."""
        self.name = name
        self.logger = configure_logger('Container', name)
        self.logger.debug("Init container")

    def start(self):
        """Start container."""
        self.logger.debug("Starting container")
        self.container = DOCKER.containers.run(
            DOCKER_IMAGE, 'sleep infinity', detach=True, working_dir='/root/workspace'
        )
        self.logger.debug("Started %s (%s)" % (self.container.name, self.container.id))

    def run(self, command, raise_when_error=True):
        """
        Run a command on the container.

        If raise_when_error=True, Exception is raised when command's exit_code != 0
        """
        self.logger.debug("Running `%s`" % (sanitize_string(command)))
        exit_code, output = self.container.exec_run(command)
        output = output.decode('UTF-8')
        self.logger.debug("Run output: %s" % (sanitize_string(output)))

        if raise_when_error and exit_code != 0:
            raise Exception(output)

        return exit_code, output

    def add_keys(self):
        """Add the necessary keys to the container in order to be able to push."""
        self.logger.debug("Adding keys")
        self.run('''bash -c 'echo -e "machine salsa.debian.org\nlogin salsa-pipeline-guest\npassword {}" > ~/.netrc'  '''.format(SALSA_PIPELINE_PASSWORD))

    def stop(self):
        """Stop the container."""
        self.logger.debug("Stopping")
        self.container.stop()
        self.container.remove(force=True)

    def __enter__(self):
        """Enter."""
        self.start()
        self.add_keys()
        return self

    def __exit__(self, type, value, traceback):
        """Exit."""
        self.stop()


class DownstreamUpdater():
    """DownstreamUpdater class."""

    def __init__(self, project_name, virt):
        """Init."""
        self.project_name = project_name
        self.virt = virt
        self.logger = configure_logger('Updater', project_name)

    @property
    @lru_cache(maxsize=None)
    def project_id(self):
        """
        Return project_id from project_name.

        Workaround because python-gitlab lib is failing when
        retreiving a project by name in Salsa.
        """
        import requests
        import urllib.parse

        url = f'{GITLAB_URL}/api/v4/projects/{urllib.parse.quote_plus(self.project_name)}'
        response = requests.get(url)
        return response.json()['id']

    @property
    @lru_cache(maxsize=None)
    def project(self):
        """Returns gitlab.Gitlab.projects.get() cached."""
        return SALSA.projects.get(self.project_id)

    def run(self):
        """
        Run the upstream check.

        Clone the project, run uscan and check if a new version is available.
        If a new version exists, try to open a merge request on the project.

        If something fails, try to open an issue.
        """
        self.logger.info("Checking")

        self.virt.run(f"gbp clone {self.project.http_url_to_repo} .")
        exit_code, output = self.virt.run(
            "gbp import-orig --uscan --no-interactive",
            raise_when_error=False
        )

        if exit_code == EC_UPDATE_AVAILABLE:
            self.logger.info("Upstream available")

            try:
                self.propose_update()
            except Exception as e:
                self.open_issue(e)

        elif exit_code == EC_UP_TO_DATE:
            self.logger.info("Up to date")

        else:
            self.open_issue(output)

    def open_issue(self, body):
        """Open an issue on the project."""
        self.logger.debug("Opening issue with body: \n %s" % body)

        if not self.project.issues_enabled:
            self.logger.error("Issues are disabled")
            return

        if args.dry:
            self.logger.info("*** Dry run. Bye ***")
            return

        if self.project.issues.list(labels=[SALSA_PIPELINE_BOT_LABEL], state='opened'):
            self.logger.info("Issue already exists. Exiting.")
            return

        self.logger.info("Creating issue")
        data = {
            'title': 'Problem found when importing upstream',
            'description': ISSUE_BODY.format(body),
            'labels': [SALSA_PIPELINE_BOT_LABEL],
        }
        self.project.issues.create(data)

    def propose_update(self):
        """Generate changelog, version number and open a merge request."""
        version = self.generate_changelog()
        branch_name = '{}_{}'.format(UPSTREAM_BRANCH, version)

        if args.dry:
            self.logger.info("*** Dry run. Bye ***")
            return

        if self.project.mergerequests.list(labels=[SALSA_PIPELINE_BOT_LABEL], state='opened'):
            self.logger.info("MR already exists. Exiting.")
            return

        commit_message = f'New upstream release {version}'
        self._commit(branch_name, commit_message, 'debian/changelog')
        self._push(branch_name)
        self._create_mr(branch_name, self.project.default_branch, commit_message)

    def generate_changelog(self):
        """Generate changelog using dch."""
        self.virt.run('gbp dch')
        exit_code, stdout = self.virt.run('dpkg-parsechangelog -S Version')
        version = stdout.strip()
        self.logger.info("New version %s" % version)
        return version

    def _commit(self, branch_name, commit_message, filename):
        """Commit filename to branch_name with commit_message."""
        self.virt.run(f'git checkout -b {branch_name}')
        self.virt.run(f'git commit -m "{commit_message}" {filename}')

    def _push(self, branch_name):
        """Push branch_name."""
        self.logger.info("Pushing to origin")
        self.virt.run(f'gbp push --debian-branch={branch_name}')
        # Sometimes `gbp push` is not enough :/
        self.virt.run(f'git push origin {branch_name}')
        self.virt.run(f'git push origin upstream')
        self.virt.run(f'git push origin pristine-tar')

    def _create_mr(self, source_branch, target_branch, title):
        """Create merge request from source_branch to target_branch."""
        self.logger.info("Creating MR %s -> %s" % (source_branch, target_branch))
        data = {
            'source_branch': source_branch,
            'target_branch': target_branch,
            'remove_source_branch': True,
            'title': title,
            'description': 'Please review this MR to update your project to latest version',
            'labels': [SALSA_PIPELINE_BOT_LABEL],
        }
        self.project.mergerequests.create(data)


def configure_logger(cls, name):
    """Configure logger."""
    logger = logging.getLogger(cls + name)
    logger.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(os.environ.get('LOG_LEVEL', logging.DEBUG))
    formatter = logging.Formatter(f'{name} - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    return logger


def configure_parser():
    """Configure parser."""
    global args
    parser = argparse.ArgumentParser()
    parser.add_argument('--dry', action='store_true', help='Don\'t push it, boy.')
    parser.add_argument('--list', action='store_true', default=LIST_FILE,
                        help='Where to get the list from. Default: %s' % LIST_FILE)
    args = parser.parse_args()


def load_projects():
    """Return iterator on projects."""
    with open(args.list, 'r') as repos:
        for project in repos.readlines():
            if project.startswith("#"):
                continue

            yield project.strip()


def main():
    """Main."""
    for project in load_projects():
        with Container(project) as virt:
            DownstreamUpdater(project, virt).run()


if __name__ == '__main__':
    configure_parser()

    main()
